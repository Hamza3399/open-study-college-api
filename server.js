const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require("mongoose");
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const Router = require("./endpoints")

// defining the Express app
const app = express();

// adding Helmet to enhance your Rest API's security
app.use(helmet());

// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json());

// enabling CORS for all requests
app.use(cors());

// adding morgan to log HTTP requests
app.use(morgan('combined'));

// Database access variables
const username = "Hamza";
const password = "dbPassword";
const cluster = "cluster0.z3bgj";
const dbname = "Open-Study-College";

//Connection to database
mongoose.connect(
    `mongodb+srv://${username}:${password}@${cluster}.mongodb.net/${dbname}?retryWrites=true&w=majority`, 
    {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
  );

//Check if database is connected correctly and if any error found
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
  console.log("Connected successfully");
});

//Initialising app for the APIs for the endpoints
app.use(Router);

// starting the server
app.listen(3000, () => {
  console.log('listening on port 3000');
});
