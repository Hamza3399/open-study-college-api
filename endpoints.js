const express = require("express");
const models = require("./models");
const app = express();

const courseModel = models.Course;
const cartModel = models.Cart;

var ObjectId = require('mongodb').ObjectId; 

// adding a course to the database
app.post("/add_course", async (request, response) => {
    const course = new courseModel(request.body);
    try {
      await course.save();
      response.send(course);
    } catch (error) {
      response.status(500).send(error);
    }
});

// adding a cart to the database
app.post("/add_cart", async (request, response) => {
  const cart = new cartModel(request.body);
  try {
    await cart.save();
    response.send(course);
  } catch (error) {
    response.status(500).send(error);
  }
});

//viewing all courses in the database
app.get("/courses", async (request, response) => {
    const courses = await courseModel.find({});
    try {
      response.send(courses);
    } catch (error) {
      response.status(500).send(error);
    }
  });

// getting course based on course ID
app.get("/courseById", async (request, response) => {
  const courses = await courseModel.find({_id: ObjectId("629f90bdc1939d7870c032be")})
  try {
    response.send(courses);
  } catch (error) {
    response.status(500).send(error);
  }
});  

//viewing all carts in the database
app.get("/carts", async (request, response) => {
  const carts = await cartModel.find({});
  try {
    response.send(carts);
  } catch (error) {
    response.status(500).send(error);
  }
});

module.exports = app;
