Open Study College Assessment

Set-up:
 Run command "node server.js"
 Open Postman and run APIs which are being tested. 

 i.e. select POST in Postman and run http://localhost:3000/add_course to add course with raw JSON data and Database is populated

I was unable to do most of this project as it required a lot of learning of new things on my part.
This is the first time I have creted an application of this sort from scratch using NodeJS so I decided to follow tutorials online and fit pieces together.

I have spent the alloted time on this project and wanted to respect the request of 4 hours being spent on this to accuratley show my attempt.

Deviations from production environment include:
- leaving console messages in code for testing
- creating database with obvious username and password
- including unused code in final product
- using Postman to alter database and interact with endpoints

How I would finish:
My next steps were going to be to finish creating all the endpoint interactions. 
This would then allow me to create a frontend to interact with and not make the use of the APIs complicated.