const mongoose = require("mongoose");

//Courses data structure for the database
const CoursesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
  cost: {
    type: Number,
    required: true,
  },
});

const Course = mongoose.model("Course", CoursesSchema);

//Carts data structure for the database which includes an array of courses. Price is not set to sum of course prices.
const CartsSchema = new mongoose.Schema({
  buyerName: {
    type: String,
    required: true,
  },
  courses: [{
    type: CoursesSchema,
    required: true,
  }],
  cost: {
    type: Number,
    required: true,
  },
});

const Cart = mongoose.model("Cart", CartsSchema);

module.exports = {Course, Cart}